
import boto3
import os
import config_file

class Good_data_s3:


    def __init__(self):

        self.path_good='Training_Raw_files/Good_data'
        self.path_bad = 'Training_Raw_files/Bad_data'

    def good_data_upload(self):

        s3 = boto3.client('s3')
        s3 = boto3.resource(
            service_name='s3',
            aws_access_key_id=config_file.access_key,
            aws_secret_access_key=config_file.secret_access_key
        )

        for i in os.listdir(self.path_good):
            path = 'Training_Raw_files/Good_data/' + str(i)
            s3.Bucket('insurance-premium-prediction').upload_file(Filename=path, Key=str(i))

    def bad_data_upload(self):

        s3 = boto3.client('s3')
        s3 = boto3.resource(
            service_name='s3',
            aws_access_key_id=config_file.access_key,
            aws_secret_access_key=config_file.secret_access_key
        )

        for i in os.listdir(self.path_bad):
            path = 'Training_Raw_files/Bad_data/' + str(i)
            s3.Bucket('ineurondatasets').upload_file(Filename=path, Key=str(i))

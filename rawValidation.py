import config_file
import os
import re
import shutil

class Raw_Data_validation:

    def __init__(self):
        pass

    def valuesFromSchema(self):
        File_name=config_file.csv_file_name
        LengthOfDateStampInFile=config_file.Date_length
        LengthOfTimeStampInFile=config_file.Time_length
        NumberofColumns=config_file.Column_count
        Column_names=config_file.Columns_Names
        return File_name,LengthOfDateStampInFile, LengthOfTimeStampInFile, Column_names, NumberofColumns

    def csv_name_validation(self):
        path="Training_Batch_Files"
        for files in os.listdir(path):
            if(len(files)==config_file.csv_file_name_len):
                split_by_csv = re.split('.csv', files)
                split_by_underscore = (re.split('_', split_by_csv[0]))
                if(split_by_underscore[0]=='incomeData'):
                    shutil.copy("Training_Batch_Files/" + files, "Training_Raw_files/Good_data")
                else:
                    shutil.copy("Training_Batch_Files/" + files, "Training_Raw_files/Bad_data")
            else:
                shutil.copy("Training_Batch_Files/" + files, "Training_Raw_files/Bad_data")




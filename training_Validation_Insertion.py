from Folder_creater import folder_creater
from Good_data_to_s3 import Good_data_s3
from rawValidation import Raw_Data_validation

'''
    The main purpose of this method is to call all the methods which are written.
    From here,this class and method are called in main method

'''


class train_validation:

    def __init__(self):
        self.raw_data = Raw_Data_validation()
        self.data_to_s3=Good_data_s3()
        self.dicrectory=folder_creater()

    def train_validation(self):

        try:
            File_name, LengthOfDateStampInFile, LengthOfTimeStampInFile, Column_names, NumberofColumns = self.raw_data.valuesFromSchema()
            self.dicrectory.folder()
            self.raw_data.csv_name_validation()
            self.data_to_s3.bad_data_upload()
            self.data_to_s3.good_data_upload()



        except Exception as e:
            raise e









